package courier

import (
	"errors"
	"reflect"
	"sync"
)

// HandlerFunc handler type for message
// HandlerFunc must be a function that take pointer to something and return an error
// Example:
//
// type Query {
//     Text string
//     Response bool
// }
//
// var fn HandlerFunc = func(q *Query) error { return nil }
//
type HandlerFunc interface{}

// Msg type
type Msg interface{}

// Courier is object that deliver msg to interested objects
type Courier interface {
	Dispatch(msg Msg) error
	AddHandler(handler HandlerFunc)
	AddEventListener(handler HandlerFunc)
	RemoveHandlerByMessage(msg Msg)
	RemoveEventListenerByMessage(msg Msg)
	ClearAll()
	Publish(msg Msg) error
}

// ErrNoHandler error
var ErrNoHandler = errors.New("handler not found")

type inProcCr struct {
	sync.RWMutex
	handlers  map[string]HandlerFunc
	listeners map[string][]HandlerFunc
}

var defaultCr = New()

// New returns new courier
func New() Courier {
	cr := &inProcCr{}
	cr.handlers = make(map[string]HandlerFunc)
	cr.listeners = make(map[string][]HandlerFunc)
	return cr
}

// Dispatch will dispatch msg to one interested object
func (c *inProcCr) Dispatch(msg Msg) error {
	var msgName = reflect.TypeOf(msg).Elem().Name()

	c.RLock()
	var handler = c.handlers[msgName]
	c.RUnlock()
	if handler == nil {
		return ErrNoHandler
	}

	var params = make([]reflect.Value, 1)
	params[0] = reflect.ValueOf(msg)

	ret := reflect.ValueOf(handler).Call(params)
	err := ret[0].Interface()
	if err == nil {
		return nil
	}
	return err.(error)
}

// AddHandler will add handler for some message type
func (c *inProcCr) AddHandler(handler HandlerFunc) {
	handlerType := reflect.TypeOf(handler)
	queryTypeName := handlerType.In(0).Elem().Name()
	c.Lock()
	c.handlers[queryTypeName] = handler
	c.Unlock()
}

// AddEventListener will add handler for listening to an event
func (c *inProcCr) AddEventListener(handler HandlerFunc) {
	handlerType := reflect.TypeOf(handler)
	eventName := handlerType.In(0).Elem().Name()
	c.Lock()
	_, exists := c.listeners[eventName]
	if !exists {
		c.listeners[eventName] = make([]HandlerFunc, 0)
	}
	c.listeners[eventName] = append(c.listeners[eventName], handler)
	c.Unlock()
}

// Publish will publish msg to all interested listeners
func (c *inProcCr) Publish(msg Msg) error {
	msgName := reflect.TypeOf(msg).Elem().Name()
	c.RLock()
	listeners := c.listeners[msgName]
	c.RUnlock()

	params := make([]reflect.Value, 1)
	params[0] = reflect.ValueOf(msg)

	for _, listenerHandler := range listeners {
		ret := reflect.ValueOf(listenerHandler).Call(params)
		if err := ret[0].Interface(); err != nil {
			return err.(error)
		}
	}
	return nil
}

// RemoveHandlerByMessage will remove handler for a given message type
func (c *inProcCr) RemoveHandlerByMessage(msg Msg) {
	value := reflect.ValueOf(msg)
	var msgName string
	switch value.Kind() {
	case reflect.Ptr:
		msgName = value.Type().Elem().Name()
	case reflect.Struct:
		msgName = value.Type().Name()
	}
	if msgName != "" {
		c.Lock()
		delete(c.handlers, msgName)
		c.Unlock()
	}
}

// RemoveEventListenerByMessage remove all event listener handlers
// for a given message type
func (c *inProcCr) RemoveEventListenerByMessage(msg Msg) {
	value := reflect.ValueOf(msg)
	var msgName string
	switch value.Kind() {
	case reflect.Ptr:
		msgName = value.Type().Elem().Name()
	case reflect.Struct:
		msgName = value.Type().Name()
	}
	if msgName != "" {
		c.Lock()
		delete(c.listeners, msgName)
		c.Unlock()
	}
}

// ClearAll will clear all handlers and event listeners
func (c *inProcCr) ClearAll() {
	c.Lock()
	c.handlers = make(map[string]HandlerFunc)
	c.listeners = make(map[string][]HandlerFunc)
	c.Unlock()
}

// Package level functions

// Dispatch will dispatch msg to one interested object
func Dispatch(msg Msg) error {
	return defaultCr.Dispatch(msg)
}

// AddHandler will add handler for some message type
func AddHandler(handler HandlerFunc) {
	defaultCr.AddHandler(handler)
}

// AddEventListener will add handler for listening to an event
func AddEventListener(handler HandlerFunc) {
	defaultCr.AddEventListener(handler)
}

// Publish will publish msg to all interested parties
func Publish(msg Msg) error {
	return defaultCr.Publish(msg)
}

// ClearAll will clear all handlers and event listeners
func ClearAll() {
	defaultCr.ClearAll()
}

// RemoveHandlerByMessage will remove handler for some message type
func RemoveHandlerByMessage(handler HandlerFunc) {
	defaultCr.RemoveHandlerByMessage(handler)
}

// RemoveEventListenerByMessage remove all event listener handlers
// for a given message type
func RemoveEventListenerByMessage(handler HandlerFunc) {
	defaultCr.RemoveEventListenerByMessage(handler)
}
