package courier

import (
	"errors"
	"fmt"
	"testing"
)

type Query struct {
	ID       int64
	Response string
}

func TestQueryHandlerReturnError(t *testing.T) {
	crr := New()

	handlerErr := errors.New("an error occured")

	crr.AddHandler(func(query *Query) error {
		return handlerErr
	})

	if err := crr.Dispatch(&Query{}); err == nil || err != handlerErr {
		t.Fatalf("expect error %v, got %v", handlerErr, err)
	}
}

func TestQueryHandlerReturn(t *testing.T) {
	crr := New()

	resp := "a response"

	crr.AddHandler(func(query *Query) error {
		query.Response = fmt.Sprintf("%s %d", resp, query.ID)
		return nil
	})

	query := &Query{ID: 100}

	if err := crr.Dispatch(query); err != nil {
		t.Fatalf("expect error nil, got error %v", err)
	} else if query.Response != fmt.Sprintf("%s %d", resp, query.ID) {
		t.Fatalf("expect response to be %v, got %v", fmt.Sprintf("%s %d", resp, query.ID), query.Response)
	}
}

func TestEventListeners(t *testing.T) {
	crr := New()
	counter := 0

	adder := []int{20, 100}

	crr.AddEventListener(func(query *Query) error {
		counter += adder[0]
		return nil
	})

	crr.AddEventListener(func(query *Query) error {
		counter += adder[1]
		return nil
	})

	if err := crr.Publish(&Query{}); err != nil {
		t.Fatalf("expect error nil, got error %v", err)
	} else if counter != adder[0]+adder[1] {
		t.Fatalf("expect publish to make counter value %d, got %d", adder[0]+adder[1], counter)
	}
}

func TestClearAll(t *testing.T) {
	crr := New()

	eventCounter := 0
	crr.AddEventListener(func(query *Query) error {
		eventCounter++
		return nil
	})

	crr.AddHandler(func(query *Query) error {
		return nil
	})

	if err := crr.Publish(&Query{}); err != nil {
		t.Fatalf("expect error nil, got error %v", err)
	} else if eventCounter != 1 {
		t.Fatalf("expect publish to make counter value %d, got %d", 1, eventCounter)
	}

	if err := crr.Dispatch(&Query{}); err != nil {
		t.Fatalf("expect error nil, got error %v", err)
	}

	crr.ClearAll()

	if err := crr.Publish(&Query{}); err != nil {
		t.Fatalf("expect error nil, got error %v", err)
	} else if eventCounter != 1 {
		t.Fatalf("expect counter value stay %d when publish, got %d", 1, eventCounter)
	}

	if err := crr.Dispatch(&Query{}); err == nil || err != ErrNoHandler {
		t.Fatalf("expect error to be %v, got %v", ErrNoHandler, err)
	}
}

func TestRemoveHandlerByMessageWithPointerMessage(t *testing.T) {
	crr := New()
	crr.AddHandler(func(query *Query) error {
		return nil
	})
	if err := crr.Dispatch(&Query{}); err != nil {
		t.Fatalf("expect error nil, got error %v", err)
	}

	crr.RemoveHandlerByMessage(&Query{})

	if err := crr.Dispatch(&Query{}); err == nil || err != ErrNoHandler {
		t.Fatalf("expect error to be %v, got %v", ErrNoHandler, err)
	}
}

func TestRemoveHandlerByMessageWithNonPointerMessage(t *testing.T) {
	crr := New()
	crr.AddHandler(func(query *Query) error {
		return nil
	})
	if err := crr.Dispatch(&Query{}); err != nil {
		t.Fatalf("expect error nil, got error %v", err)
	}

	crr.RemoveHandlerByMessage(Query{})

	if err := crr.Dispatch(&Query{}); err == nil || err != ErrNoHandler {
		t.Fatalf("expect error to be %v, got %v", ErrNoHandler, err)
	}
}

func TestRemoveEventListenerByMessageWithPointerMessage(t *testing.T) {
	crr := New()
	eventCounter := 0
	crr.AddEventListener(func(query *Query) error {
		eventCounter++
		return nil
	})
	if err := crr.Publish(&Query{}); err != nil {
		t.Fatalf("expect error nil, got error %v", err)
	} else if eventCounter != 1 {
		t.Fatalf("expect publish to make counter value %d, got %d", 1, eventCounter)
	}

	crr.RemoveEventListenerByMessage(&Query{})

	if err := crr.Publish(&Query{}); err != nil {
		t.Fatalf("expect error nil, got error %v", err)
	} else if eventCounter != 1 {
		t.Fatalf("expect counter value stay %d when publish, got %d", 1, eventCounter)
	}
}

func TestRemoveEventListenerByMessageWithNonPointerMessage(t *testing.T) {
	crr := New()
	eventCounter := 0
	crr.AddEventListener(func(query *Query) error {
		eventCounter++
		return nil
	})
	if err := crr.Publish(&Query{}); err != nil {
		t.Fatalf("expect error nil, got error %v", err)
	} else if eventCounter != 1 {
		t.Fatalf("expect publish to make counter value %d, got %d", 1, eventCounter)
	}

	crr.RemoveEventListenerByMessage(Query{})

	if err := crr.Publish(&Query{}); err != nil {
		t.Fatalf("expect error nil, got error %v", err)
	} else if eventCounter != 1 {
		t.Fatalf("expect counter value stay %d when publish, got %d", 1, eventCounter)
	}
}
